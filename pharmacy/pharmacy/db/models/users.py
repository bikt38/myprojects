from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import INTEGER, TEXT
from sqlalchemy.orm import relationship

from pharmacy.db import DeclarativeBase


class UserBaseStorage(DeclarativeBase):
    __abstract__ = True

    id = Column(
        INTEGER,
        primary_key=True,
        unique=True,
        doc="Unique id of the integer in table",
    )
    full_name = Column(
        TEXT,
        doc="Full name of the user",
    )
    phone = Column(
        TEXT,
        unique=True,
        doc="Phone of the user",
    )
    password_hash = Column(
        TEXT,
        doc="Password hash of the user",
    )

    def __repr__(self):
        columns = {column.name: getattr(self, column.name) for column in self.__table__.columns}
        return f'<{self.__tablename__}: {", ".join(map(lambda x: f"{x[0]}={x[1]}", columns.items()))}>'


class UserAccountStorage(UserBaseStorage):
    __tablename__ = "user_account"


class DoctorAccountStorage(UserBaseStorage):
    __tablename__ = "doctor_account"

    specialty_id = Column(
        INTEGER,
        ForeignKey("specialty.id")
    )

