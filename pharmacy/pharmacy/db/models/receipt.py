from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import INTEGER, NUMERIC, TEXT
from sqlalchemy.orm import relationship

from pharmacy.db import DeclarativeBase


class ReceiptStorage(DeclarativeBase):
    __tablename__ = "receipt"

    id = Column(
        INTEGER,
        primary_key=True,
        unique=True,
        doc="Unique id of the integer in table",
    )

    user_id = Column(
        INTEGER,
        ForeignKey("user_account.id"),
        doc="User id of the receipt",
    )
    item_id = Column(
        INTEGER,
        ForeignKey("receipt_item.id"),
        doc="Item id of the receipt",
    )

    def __repr__(self):
        columns = {column.name: getattr(self, column.name) for column in self.__table__.columns}
        return f'<{self.__tablename__}: {", ".join(map(lambda x: f"{x[0]}={x[1]}", columns.items()))}>'