from sqlalchemy import Column, ForeignKey
from sqlalchemy.dialects.postgresql import INTEGER, NUMERIC, TEXT
from sqlalchemy.orm import relationship

from pharmacy.db import DeclarativeBase


class ItemBaseStorage(DeclarativeBase):
    __abstract__ = True

    id = Column(
        INTEGER,
        primary_key=True,
        unique=True,
        doc="Unique id of the integer in table",
    )
    name = Column(
        TEXT,
        nullable=False,
        doc="Name of the item",
    )
    amount = Column(
        INTEGER,
        nullable=False,
        doc="Amount of the item",
    )
    price = Column(
        NUMERIC(8, 2),
        nullable=False,
        doc="Price of the item",
    )
    dosage_form = Column(
        TEXT,
        doc="Dosage form of the item",
    )
    manufacturer = Column(
        TEXT,
        doc="Manufacturer of the item",
    )
    barcode = Column(
        TEXT,
        unique=True,
        doc="Barcode of the item",
    )

    def __repr__(self):
        columns = {column.name: getattr(self, column.name) for column in self.__table__.columns}
        return f'<{self.__tablename__}: {", ".join(map(lambda x: f"{x[0]}={x[1]}", columns.items()))}>'


class ItemCommonStorage(ItemBaseStorage):
    __tablename__ = "common_item"


class ItemReceiptStorage(ItemBaseStorage):
    __tablename__ = "receipt_item"


class ItemSpecialStorage(ItemBaseStorage):
    __tablename__ = "special_item"

    specialty_id = Column(
        INTEGER,
        ForeignKey("specialty.id")
    )
