import abc
from typing import Type

from pharmacy.db.models.items import (ItemBaseStorage, ItemCommonStorage,
                                      ItemReceiptStorage, ItemSpecialStorage)
from pharmacy.schemas.items import (ItemBase, ItemCommon, ItemReceipt,
                                    ItemSpecial)


class ItemType(abc.ABC):
    @abc.abstractmethod
    def get_schemas(self) -> Type[ItemBase]:
        raise NotImplemented

    @abc.abstractmethod
    def get_table(self) -> Type[ItemBaseStorage]:
        raise NotImplemented

    @abc.abstractmethod
    def get_name(self) -> str:
        raise NotImplemented


class ItemCommonType(ItemType):
    def get_schemas(self) -> Type[ItemBase]:
        return ItemCommon

    def get_table(self) -> Type[ItemBaseStorage]:
        return ItemCommonStorage

    def get_name(self) -> str:
        return "ItemCommon"


class ItemReceiptType(ItemType):
    def get_schemas(self) -> Type[ItemBase]:
        return ItemReceipt

    def get_table(self) -> Type[ItemBaseStorage]:
        return ItemReceiptStorage

    def get_name(self) -> str:
        return "ItemReceipt"


class ItemSpecialType(ItemType):
    def get_schemas(self) -> Type[ItemBase]:
        return ItemSpecial

    def get_table(self) -> Type[ItemBaseStorage]:
        return ItemSpecialStorage

    def get_name(self) -> str:
        return "ItemSpecial"


def select_type_product(id: str) -> ItemType:
    category = id.split("_")[0]
    if category == "common":
        return ItemCommonType()
    elif category == "receipt":
        return ItemReceiptType()
    elif category == "special":
        return ItemSpecialType()
    else:
        raise ValueError("WRONG_CATEGORY")


