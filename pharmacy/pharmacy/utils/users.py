import abc
from typing import Optional, Type

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from pharmacy.db.models import (DoctorAccountStorage, ReceiptStorage,
                                SpecialtyStorage, UserAccountStorage)
from pharmacy.db.models.users import UserBaseStorage
from pharmacy.schemas import ItemBase
from pharmacy.schemas.users import DoctorAccount, UserAccount, UserBase
from pharmacy.utils.items import ItemType


class UserClient(abc.ABC):
    @abc.abstractmethod
    def get_schemas(self) -> Type[UserBase]:
        raise NotImplemented

    @abc.abstractmethod
    def get_table(self) -> Type[UserBaseStorage]:
        raise NotImplemented

    @abc.abstractmethod
    async def get_permissions(self, item: ItemBase, user_id: int, session: AsyncSession) -> None:
        raise NotImplemented


class UserAccountClient(UserClient):
    def get_schemas(self) -> Type[UserBase]:
        return UserAccount

    def get_table(self) -> Type[UserBaseStorage]:
        return UserAccountStorage

    async def get_permissions(self, item: ItemBase, user_id: int, session: AsyncSession) -> None:
        if item.item_type == "ItemCommon":
            return None
        elif item.item_type == "ItemReceipt":
            exist = await session.execute(select(ReceiptStorage).where(ReceiptStorage.user_id == user_id).where(ReceiptStorage.item_id == item.id))
            if exist.scalars().first() is None:
                raise ValueError("NO_RECEIPT")
            return None
        elif item.item_type == "ItemSpecial":
            raise ValueError("ITEM_IS_SPECIAL")
        raise ValueError("No item type")


class DoctorAccountClient(UserClient):
    def get_schemas(self) -> Type[UserBase]:
        return DoctorAccount

    def get_table(self) -> Type[UserBaseStorage]:
        return DoctorAccountStorage

    async def get_permissions(self, item: ItemBase, user_id: int, session: AsyncSession) -> None:
        if item.item_type == "ItemCommon":
            return None
        elif item.item_type == "ItemReceipt":
            return None
        elif item.item_type == "ItemSpecial":
            user = await session.execute(select(self.get_table()).where(self.get_table().id == user_id))
            user = self.get_schemas().from_orm(user.scalars().first())
            if user.specialty_id != item.specialty_id:
                raise ValueError("ITEM_SPECIAL_WRONG_SPECIFIC")
            return None
        raise ValueError("No item type")


class AnonimClient(UserClient):
    def get_schemas(self) -> Optional[Type[UserBase]]:
        return None

    def get_table(self) -> Optional[Type[UserBaseStorage]]:
        return None

    async def get_permissions(self, item: ItemBase,  user_id: int, session: AsyncSession) -> None:
        if item.item_type == "ItemCommon":
            raise ValueError("NO_USER")
        elif item.item_type == "ItemReceipt":
            raise ValueError("NO_USER_NO_RECEIPT")
        elif item.item_type == "ItemSpecial":
            raise ValueError("NO_USER_SPECIAL_ITEM")
        raise ValueError("No item type")


async def select_client(id: int, session: AsyncSession) -> UserClient:
    for user_type in [UserAccountClient(), DoctorAccountClient()]:
        user = await session.execute(select(user_type.get_table()).where(user_type.get_table().id == id))
        user = user.scalars().first()
        if user:
            return user_type
    return AnonimClient()
