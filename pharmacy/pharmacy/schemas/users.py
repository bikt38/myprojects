from pydantic import BaseModel, Field


class UserBase(BaseModel):
    id: int = Field(title="User ID")
    full_name: str = Field(title="Full name of the user")
    phone: str = Field(title="Phone of the user")
    password_hash: str = Field(title="Password hash of the user")

    class Config:
        orm_mode = True


class UserAccount(UserBase):
    pass


class DoctorAccount(UserBase):
    specialty_id: int = None
