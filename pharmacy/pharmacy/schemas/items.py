from __future__ import annotations

from ctypes import Union

from pydantic import BaseModel, Field


class ItemBase(BaseModel):
    id: int = Field(title="User ID")
    name: str = Field(title="Name of the item")
    amount: int = Field(title="Amount of the item")
    price: float = Field(title="Price of the item")
    dosage_form: str = Field(title="Dosage form of the item")
    manufacturer: str = Field(title="Manufacturer of the item")
    barcode: str = Field(title="Barcode of the item")
    item_type: str = None

    class Config:
        orm_mode = True


class ItemCommon(ItemBase):
    pass


class ItemReceipt(ItemBase):
    pass


class ItemSpecial(ItemBase):
    specialty_id: int = None
