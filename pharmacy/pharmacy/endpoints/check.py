import datetime
import json
from random import choice
from string import ascii_uppercase, digits
from typing import List

from fastapi import APIRouter, Body, Depends, Query
from fastapi.exceptions import HTTPException
from fastapi.openapi.models import Response
from fastapi.responses import JSONResponse, ORJSONResponse
from sqlalchemy import delete, select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from pharmacy.db.connection import get_session
from pharmacy.utils.items import select_type_product
from pharmacy.utils.users import select_client

api_router = APIRouter(tags=["Url"])


@api_router.get(
    "/check",
    status_code=status.HTTP_200_OK,
)
async def make_shorter(user_id: int, item_id: List[str] = Query(None), session: AsyncSession = Depends(get_session)):
    errors = []
    for item in item_id:
        try:
            item_type = select_type_product(item)
            if len(item.split("_")) != 2 or not item.split("_")[1].isdigit():
                raise ValueError("INCORRECT_ITEM_ID")
            item_db = await session.execute(select(item_type.get_table()).where(item_type.get_table().id == int(item.split("_")[1])))
            item_db = item_db.scalars().first()
            if not item_db:
                raise ValueError("ITEM_NOT_FOUND")
            item_schema = item_type.get_schemas().from_orm(item_db)
            item_schema.item_type = item_type.get_name()
            user_client = await select_client(user_id, session)
            await user_client.get_permissions(item_schema, user_id, session)
        except ValueError as e:
            errors.append({"item_id": item, "problem": str(e)})

    return JSONResponse(content=errors)
