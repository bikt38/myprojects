from pharmacy.endpoints.check import api_router as check

list_of_routes = [
    check,
]


__all__ = [
    "list_of_routes",
]
