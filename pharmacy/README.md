Я использовал Fastapi + SQLAlchemy + PostgreSQL для решения задачи. 

Также я обернул приложение в Docker контейнер, чтобы можно было запустить его локально.
Для запуска приложения необходимо выполнить команду:
```bash
docker-compose up
```
После этого приложение будет доступно по адресу http://localhost:8080

Для запуска тестов необходимо выполнить команду:
```bash
cd tests
poetry install --no-dev
poetry run pytest tests
```


