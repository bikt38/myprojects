"""
Сервис с REST API, позволяющий укорачивать ссылки.
"""
__author__ = "Biktimirov Timur"
__maintainer__ = __author__

__email__ = "bikt38@gmail.com"
__license__ = "MIT"
__version__ = "0.0.1"


__all__ = (
    "__author__",
    "__email__",
    "__license__",
    "__maintainer__",
    "__version__",
)
