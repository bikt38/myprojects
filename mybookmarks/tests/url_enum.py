class URLEnum:
    USER_REGISTRATION = "/api/v1/user/registration"
    USER_AUTHENTICATION = "/api/v1/user/authentication"
    USER_ME = "/api/v1/user/me"
    USER_DELETE = "/api/v1/user/takeout"

    PING_APPLICATION = "/api/v1/health_check/ping_application"
    PING_DATABASE = "/api/v1/health_check/ping_database"

    BOOKMARK_CREATE = "/api/v1/bookmark"
    BOOKMARK_GET = "/api/v1/bookmark/{bookmark_id}"
    BOOKMARK_GET_LIST = "/api/v1/bookmark"
    BOOKMARK_DELETE = "/api/v1/bookmark/{bookmark_id}"
