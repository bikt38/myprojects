from bookmarker.utils.user.business_logic import create_access_token, verify_password
from bookmarker.utils.bookmark.get_title import get_page_title
from bookmarker.utils.common.hostname import get_hostname

class TestBusinessLogic:
    async def test_create_access_token(self):
        token = create_access_token(data={"sub": "test"})
        assert token is not None

    async def test_verify_password(self):
        hash_password = verify_password("test", "$2a$12$J5SCkg4dkAFA/oay9g7wreLLH0Vx3WJso/8uU6hpu63JVKAVAi33C")
        assert hash_password is True
        hash_password = verify_password("test", "$2a$12$mPxiCfZGP9BFc8zfn3xEuO2ydoGPOrcbRYXUastmsMcPYHt4isSGq")
        assert hash_password is False

    async def test_get_page_title(self):
        title = get_page_title("https://ya.ru/")
        assert title == "Яндекс"
        title = get_page_title("https://ya.ru/404")
        assert title is None

    async def test_get_hostname(self):
        hostname = get_hostname("https://google.com")
        assert hostname == "google.com"
        hostname = get_hostname("https://google.com/404")
        assert hostname == "google.com"
