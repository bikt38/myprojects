from faker import Faker
from starlette import status

from tests.url_enum import URLEnum


class BaseFactory:
    def to_json(self):
        return self.__dict__


class UserFactory(BaseFactory):
    def __init__(self, email=None, password=None, username=None):
        self.email = email or Faker().email()
        self.password = password or Faker().password()
        self.username = username or Faker().user_name()

    async def create(self, client):
        return await client.post(URLEnum.USER_REGISTRATION, json=self.to_json())

    async def login(self, client):
        return await client.post(
            URLEnum.USER_AUTHENTICATION,
            data=self.to_json(),
            headers={"Content-Type": "application/x-www-form-urlencoded"},
        )

    async def get_jwt(self, client):
        response = await self.create(client)
        assert response.status_code == status.HTTP_201_CREATED
        response = await self.login(client)
        assert response.status_code == status.HTTP_200_OK
        assert "access_token" in response.json()
        return response.json()["access_token"]


class BookmarkFactory(BaseFactory):
    def __init__(self, link=None, tag=None):
        self.link = link or "https://ya.ru/"
        self.tag = tag or Faker().word()

    async def create(self, client, jwt_token):
        return await client.post(
            URLEnum.BOOKMARK_CREATE, json=self.to_json(), headers={"Authorization": f"Bearer {jwt_token}"}
        )

    async def get(self, client, jwt_token, bookmark_id):
        return await client.get(
            URLEnum.BOOKMARK_GET.format(bookmark_id=bookmark_id), headers={"Authorization": f"Bearer {jwt_token}"}
        )
