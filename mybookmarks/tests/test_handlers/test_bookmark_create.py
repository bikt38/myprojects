from unittest.mock import patch

from starlette import status

from tests.factories import BookmarkFactory, UserFactory


class TestBookmarkCreateHandlers:
    async def test_base_scenario(self, client):
        with patch("bookmarker.utils.bookmark.get_page_title", return_value="Яндекс"):
            user = UserFactory()
            jwt_token = await user.get_jwt(client)
            bookmark = BookmarkFactory()
            response = await bookmark.create(client, jwt_token)
            assert response.status_code == status.HTTP_201_CREATED
            assert response.json()["link"] == bookmark.link
            assert response.json()["tag"] == bookmark.tag
            assert "id" in response.json()
            assert response.json()["title"] == "Яндекс"

    async def test_wrong_token(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        bookmark = BookmarkFactory()
        response = await bookmark.create(client, jwt_token + "wrong")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_duplicate_token(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        bookmark = BookmarkFactory()
        response = await bookmark.create(client, jwt_token)
        assert response.status_code == status.HTTP_201_CREATED
        response = await bookmark.create(client, jwt_token)
        assert response.status_code == status.HTTP_201_CREATED

    async def test_404_link(self, client):
        with patch("bookmarker.utils.bookmark.get_page_title", return_value=None):
            user = UserFactory()
            jwt_token = await user.get_jwt(client)
            bookmark = BookmarkFactory()
            bookmark.link = "https://google.com/404"
            response = await bookmark.create(client, jwt_token)
            assert response.status_code == status.HTTP_400_BAD_REQUEST

    async def test_not_tag(self, client):
        with patch("bookmarker.utils.bookmark.get_page_title", return_value="Яндекс"):
            user = UserFactory()
            jwt_token = await user.get_jwt(client)
            bookmark = BookmarkFactory()
            bookmark.tag = None
            response = await bookmark.create(client, jwt_token)
            assert response.status_code == status.HTTP_201_CREATED
            assert response.json()["link"] == bookmark.link
            assert response.json()["tag"] == bookmark.tag
            assert "id" in response.json()
            assert response.json()["title"] == "Яндекс"
