from starlette import status

from tests.factories import UserFactory
from tests.url_enum import URLEnum


class TestUserTakeoutHandlers:
    @staticmethod
    def get_url() -> str:
        return URLEnum.USER_DELETE

    @staticmethod
    def get_headers(jwt_token: str) -> dict:
        return {"Authorization": f"Bearer {jwt_token}"}

    async def test_base_scenario(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        response = await client.delete(url=self.get_url(), headers=self.get_headers(jwt_token))
        assert response.status_code == status.HTTP_204_NO_CONTENT
        response = await user.login(client)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_wrong_token(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        response = await client.delete(url=self.get_url(), headers=self.get_headers(jwt_token + "wrong"))
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_duplicate_token(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        response = await client.delete(url=self.get_url(), headers=self.get_headers(jwt_token))
        assert response.status_code == status.HTTP_204_NO_CONTENT
        response = await client.delete(url=self.get_url(), headers=self.get_headers(jwt_token))
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
