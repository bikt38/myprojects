from starlette import status

from tests.factories import UserFactory


class TestUserAuthenticationHandlers:
    async def test_base_scenario(self, client):
        user = UserFactory()
        await user.create(client)
        response = await user.login(client)
        assert response.status_code == status.HTTP_200_OK
        assert "access_token" in response.json()

    async def test_wrong_password(self, client):
        user = UserFactory()
        await user.create(client)
        user.password = "wrong_password"
        response = await user.login(client)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_wrong_username(self, client):
        user = UserFactory()
        await user.create(client)
        user.username = "wrong_username"
        response = await user.login(client)
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
