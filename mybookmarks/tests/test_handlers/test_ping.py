from starlette import status

from tests.url_enum import URLEnum


class TestPingHandlers:
    async def test_application(self, client):
        response = await client.get(url=URLEnum.PING_APPLICATION)
        assert response.status_code == status.HTTP_200_OK

    async def test_database(self, client):
        response = await client.get(url=URLEnum.PING_DATABASE)
        assert response.status_code == status.HTTP_200_OK
