from starlette import status

from tests.factories import BookmarkFactory, UserFactory
from tests.url_enum import URLEnum


class TestBookmarkGetHandlers:
    @staticmethod
    def get_url(bookmark_id: str) -> str:
        return URLEnum.BOOKMARK_DELETE.format(bookmark_id=bookmark_id)

    def get_headers(self, jwt_token: str) -> dict:
        return {"Authorization": f"Bearer {jwt_token}"}

    async def test_base_scenario(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        bookmark = BookmarkFactory()
        bookmark_create = await bookmark.create(client, jwt_token)
        response = await client.delete(
            url=self.get_url(bookmark_create.json()["id"]), headers=self.get_headers(jwt_token)
        )
        assert response.status_code == status.HTTP_204_NO_CONTENT
        response = await bookmark.get(client, jwt_token, bookmark_create.json()["id"])
        assert response.status_code == status.HTTP_404_NOT_FOUND

    async def test_wrong_token(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        bookmark = BookmarkFactory()
        bookmark_create = await bookmark.create(client, jwt_token)
        response = await client.delete(
            url=self.get_url(bookmark_create.json()["id"]), headers=self.get_headers(jwt_token + "wrong")
        )
        assert response.status_code == status.HTTP_401_UNAUTHORIZED
        response = await bookmark.get(client, jwt_token, bookmark_create.json()["id"])
        assert response.status_code == status.HTTP_200_OK
