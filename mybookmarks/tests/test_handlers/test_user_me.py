from starlette import status

from tests.factories import UserFactory
from tests.url_enum import URLEnum


class TestUserMeHandlers:
    @staticmethod
    def get_url() -> str:
        return URLEnum.USER_ME

    @staticmethod
    def get_headers(jwt_token: str) -> dict:
        return {"Authorization": f"Bearer {jwt_token}"}

    async def test_base_scenario(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        response = await client.get(url=self.get_url(), headers=self.get_headers(jwt_token))
        assert response.status_code == status.HTTP_200_OK
        assert response.json()["username"] == user.username
        assert response.json()["email"] == user.email
        assert "dt_created" in response.json()
        assert "dt_updated" in response.json()
