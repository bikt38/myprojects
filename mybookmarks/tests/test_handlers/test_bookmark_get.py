from unittest.mock import patch

from starlette import status

from tests.factories import BookmarkFactory, UserFactory


class TestBookmarkGetHandlers:
    async def test_base_scenario(self, client):
        with patch("bookmarker.utils.bookmark.get_page_title", return_value="Яндекс"):
            user = UserFactory()
            jwt_token = await user.get_jwt(client)
            bookmark = BookmarkFactory()
            bookmark_create = await bookmark.create(client, jwt_token)
            response = await bookmark.get(client, jwt_token, bookmark_create.json()["id"])
            assert response.status_code == status.HTTP_200_OK
            assert response.json()["link"] == bookmark.link
            assert response.json()["tag"] == bookmark.tag
            assert response.json()["title"] == "Яндекс"
            assert "id" in response.json()

    async def test_wrong_token(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        bookmark = BookmarkFactory()
        bookmark_create = await bookmark.create(client, jwt_token)
        response = await bookmark.get(client, jwt_token + "wrong", bookmark_create.json()["id"])
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_wrong_id(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        bookmark = BookmarkFactory()
        bookmark_create = await bookmark.create(client, jwt_token)
        response = await bookmark.get(client, jwt_token, bookmark_create.json()["id"][:-4] + "0000")
        assert response.status_code == status.HTTP_404_NOT_FOUND

    async def test_wrong_id_and_token(self, client):
        jwt_token = await UserFactory().get_jwt(client)
        bookmark = BookmarkFactory()
        bookmark_create = await bookmark.create(client, jwt_token)
        response = await bookmark.get(client, jwt_token + "wrong", bookmark_create.json()["id"][:-4] + "0000")
        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_other_user(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        bookmark = BookmarkFactory()
        bookmark_create = await bookmark.create(client, jwt_token)
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        response = await bookmark.get(client, jwt_token, bookmark_create.json()["id"])
        assert response.status_code == status.HTTP_404_NOT_FOUND
