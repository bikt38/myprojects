from starlette import status

from tests.factories import UserFactory


class TestUserRegistrationHandlers:
    async def test_base_scenario(self, client):
        user = UserFactory()
        response = await user.create(client)
        assert response.status_code == status.HTTP_201_CREATED

    async def test_duplicate(self, client):
        user = UserFactory()
        response = await user.create(client)
        assert response.status_code == status.HTTP_201_CREATED
        response = await user.create(client)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    async def test_wrong_email(self, client):
        user = UserFactory(email="wrong_email")
        response = await user.create(client)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    async def test_none_password(self, client):
        user = UserFactory()
        user.password = None
        response = await user.create(client)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
