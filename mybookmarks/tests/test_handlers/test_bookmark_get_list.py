import pytest
from starlette import status

from tests.factories import BookmarkFactory, UserFactory
from tests.url_enum import URLEnum


class TestBookmarkGetListHandlers:
    @staticmethod
    def get_url() -> str:
        return URLEnum.BOOKMARK_GET_LIST

    async def create_bookes(self, client, jwt_token, count, tag=None):
        for _ in range(count):
            bookmark = BookmarkFactory(tag=tag)
            await bookmark.create(client, jwt_token)

    async def test_base_scenario(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        await self.create_bookes(client, jwt_token, 5)
        response = await client.get(self.get_url(), headers={"Authorization": f"Bearer {jwt_token}"})
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()["items"]) == 5
    async def test_pagination(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        await self.create_bookes(client, jwt_token, 5)
        response = await client.get(
            self.get_url() + "?page=2&size=10", headers={"Authorization": f"Bearer {jwt_token}"}
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()["items"]) == 0

    async def test_pagination_size(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        await self.create_bookes(client, jwt_token, 5)
        response = await client.get(self.get_url() + "?page=1&size=2", headers={"Authorization": f"Bearer {jwt_token}"})
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()["items"]) == 2

    async def test_filter_tag(self, client):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        await self.create_bookes(client, jwt_token, 5, "tag")
        await self.create_bookes(client, jwt_token, 5, "tag2")
        response = await client.get(
            self.get_url() + "?tag=tag", headers={"Authorization": f"Bearer {jwt_token}"}
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()["items"]) == 5

    @pytest.mark.parametrize(
        'sort_key,name_key',
        [
            pytest.param(
                "BY_ID", "id",
                id="wrong sort by ID",
            ),
            pytest.param(
                "BY_DATE", "dt_created",
                id="wrong sort by DATE",
            ),
            pytest.param(
                "BY_TITLE", "title",
                id="wrong sort by TITLE",
            ),
            pytest.param(
                "BY_LINK", "link",
                id="wrong sort by LINK",
            ),
        ],
    )
    async def test_sort(self, client, sort_key, name_key):
        user = UserFactory()
        jwt_token = await user.get_jwt(client)
        await self.create_bookes(client, jwt_token, 5)
        response = await client.get(
            self.get_url() + f"?sort_key={sort_key}", headers={"Authorization": f"Bearer {jwt_token}"}
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(response.json()["items"]) == 5
        for index in range(4):
            assert response.json()["items"][index][name_key] <= response.json()["items"][index + 1][name_key]
